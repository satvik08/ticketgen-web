package com.staticbyte.ticketgen;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TicketgenApplication {

	public static void main(String[] args) {
		SpringApplication.run(TicketgenApplication.class, args);
	}

}
